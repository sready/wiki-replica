# Need help?

If you need help from the sysadmin team (or even if you're not sure
which team!), please do contact us using one of the following
mechanisms:

<!-- this is a copy paste from policy/tpa-rfc-2-support.md, update -->
<!-- the other page if this is changed -->

## Quick question: chat

If you have "just a quick question" or some quick thing we can help
you with, ask us on IRC: you can find us in `#tor-admin` on
`irc.oftc.net` and in other tor channels.

It's possible we ask you to create a ticket if we're in a pinch. It's
also a good way to bring your attention to some emergency or ticket
that was filed elsewhere.

## Bug reports, feature requests and others: issue tracker

Most requests and questions should go into the issue tracker, which is
currently [GitLab][] ([direct link to a new ticket form][]). Try to find
a good label describing the service you're having a problem with, but
in doubt, just file the issue with as much details as you can.

You can also mark an issue as confidential, in which case only members
of the team (and the larger "tpo" organisation on GitLab) will be able
to read it.

[GitLab]: https://gitlab.torproject.org/tpo/tpa/team/
[direct link to a new ticket form]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new

## Private question and fallback: email

If you want to discuss a sensitive matter that requires privacy or are
unsure how to reach us, you can always write to us by email, at
[torproject-admin@torproject.org][].

[torproject-admin@torproject.org]: mailto:torproject-admin@torproject.org

For details on those options and our support policy, including support
levels, supported services and timelines, see the [TPA-RFC-2: support policy](policy/tpa-rfc-2-support).
