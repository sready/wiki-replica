Two new sysadmins were hired, so we're holding meetings again! Welcome
again to kez and lavamind, who joined us last week.

Here are the minutes from the meeting we held on June 14 and 16.

# Roll call: who's there and emergencies

 * anarcat
 * gaba
 * kez
 * lavamind

No emergencies.

# Triage & schedules

 * Introduce the triage system
   * the "triage star of the weeks" rotates every two weeks
   * the star triages the boards regularly, making sure there are no
     "Open" tickets, and assigning tickets or dealing with small
     tickets
   * see also [TPA-RFC-5][] for the labeling nomenclature
 * we were doing weekly checkins with hiro during the handover on
   wednesday, since we were both part time
 * work schedules:
   * j: monday, tuesday - full day; wednesday - partially
   * kez: flexible - TBD
   * anarcat: monday to thursday - full day

[TPA-RFC-5]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-5-gitlab#roadmap-gitlab-boards

# Communication in the team

What is expected? When to bring it in IRC versus email versus ticket?
Acknowledgements.

 * we expect people to update tickets when they work on them
 * we expect acknowledgements when people see their names mentions on
   IRC

# Short term planning: anarcat going AFK

This basically involves making sure our new hires have enough work
while they are gone.

We reviewed the Doing/Next columns and assign issues in the [TPA
board][] and [web board][].

[web board]: https://gitlab.torproject.org/groups/tpo/web/-/boards
[TPA board]: https://gitlab.torproject.org/groups/tpo/tpa/-/boards

We also reviewed the letter anarcat later sent to `tor-internal@`
(private, not linked here).

Then the meeting paused after one hour.

When we returned on wednesday, we jumped to the roadmap review
(below), and then returned here to briefly review the Backlog. 

We reviewed anarcat's queue to make sure things would be okay after he
left, and also made sure kez and lavamind had enough work. gaba will
make sure they are assigned work from the Backlog as well.

# Roadmap review

Review and prioritize:

 * [yearly TPA roadmap][]
 * web
   * [board][]
   * [priorities][]

[yearly TPA roadmap]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2021
[board]: board
[priorities]: https://gitlab.torproject.org/tpo/web/team

## Web priorities allocations (sort out by priorities)

We reviewed the priorities page and made sure we had most of the stuff
covered. We don't assign tasks directly in the wiki page, but we did a
tentative assignation pass here:

 * Donations page redesign (support to Openflows) - kez
 * Onion Services v2 deprecation support - lavamind
 * Improves bridges.torproject.org - kez
 * Remove outdated documentation from the header - kez & gus
 * Migrate blog.torproject.org from Drupal To Lektor: it needs a
   milestone and planning - lavamind
 * Support forum  - lavamind
 * Developer portal - lavamind & kez
 * Get website build from jenkins into to gitlabCI for the static
   mirror pool (before December) - kez
 * Get up to speed on maintenance tasks:
   * Bootstrap upgrade - lavamind
   * browser documentation update (this is content and mostly is on
     gus's plate) gus
   * get translation stats available - kez
   * rename 'master' branch as 'main' - lavamind
   * fix wiki for documentation - gaba
   * get onion service tooling into tpo gitlab namespace - lavamind

## TPA roadmap review

We reviewed the TPA roadmap for the first time since the beginning of
the year, which involved going through the first two quarters to
identify what was done and missed. We also established the priorities
for Q3 and Q4. Those changes are mostly contained in [this commit on
the wiki][].

[this commit on the wiki]: https://gitlab.torproject.org/tpo/tpa/wiki-replica/-/commit/5968075c75d4249b16afdc9724bc277821b5f7d6

# Other discussions

No new item came up in the meeting, which already was extended an
extra hour to cover for the extra roadmap work.

# Next meeting

 * we do quick checkin on monday 14 UTC 10 eastern, at the beginning
   of the office hours (UPDATE: we're pushing that to later in the
   day, to 10:00 US/Pacific, 14:00 America/Montevideo, 13:00
   US/Eastern, 17:00 UTC, 19:00 Europe/Paris)
 * we do monthly meetings instead of checkins on the first monday of
   the month

# Metrics of the month

Those were sent on June 2nd, it would be silly to send them again.
