We keep minutes of our meetings here.

We hold the following regular meetings:

 * office hours: an open (to tor-internal) videoconferencing hangout
   every Monday during business hours
 * weekly check-in: every Monday at 17:00 UTC, which is:
   * during daylight savings: 10:00 US/Pacific, 14:00
     America/Montevideo, 13:00 US/Eastern, 19:00 Europe/Paris,
   * normal time: 09:00 US/Pacific, 14:00 America/Montevideo, 12:00
     US/Eastern, 18:00 Europe/Paris
 * monthly meetings: every first check-in (that is, every first
   Monday) of the month is a formal meeting with minutes, listed below

Those are just for TPA, there are broader notes on meetings in the
[organization Meetings page](https://gitlab.torproject.org/tpo/team/-/wikis/Meetings).

[[_TOC_]]

<!-- a full list can be found with: -->

<!-- ls -d meeting/*.md | sed 's/.md$//;s/\(.*\)/ * [\1](\1)/' -->

<!-- but this is really just manually  curated now -->

<!-- and yes, those are HTML comments in a markdown document, -->
<!-- amazingly, this does what you would expect. or maybe not. -->

# 2022

 * [2022-06-21](meeting/2022-06-21)
 * [2022-06-06](meeting/2022-06-06)
 * [2022-05-09](meeting/2022-05-09)
 * [2022-04-04](meeting/2022-04-04)
 * [2022-03-14](meeting/2022-03-14)
 * [2022-02-14](meeting/2022-02-14)
 * [2022-01-24](meeting/2022-01-24)
 * [2022-01-11](meeting/2022-01-11)

# 2021

 * [2021-12-06](meeting/2021-12-06)
 * [2021-11-01](meeting/2021-11-01)
 * [2021-10-07](meeting/2021-10-07)
 * [2021-09-07](meeting/2021-09-07)
 * [2021-06-14](meeting/2021-06-14)
 * [2021-06-02](meeting/2021-06-02) (report only)
 * [2021-05-03](meeting/2021-05-03) (report only)
 * [2021-04-07](meeting/2021-04-07) (report only)
 * [2021-03-02](meeting/2021-03-02)
 * [2021-02-02](meeting/2021-02-02)
 * [2021-01-26](meeting/2021-01-26)
 * [2021-01-19](meeting/2021-01-19)

# 2020

 * [2020-12-02](meeting/2020-12-02)
 * [2020-11-18](meeting/2020-11-18)
 * [2020-07-01](meeting/2020-07-01)
 * [2020-06-10](meeting/2020-06-10)
 * [2020-05-11](meeting/2020-05-11)
 * [2020-04-14](meeting/2020-04-14)
 * [2020-03-09](meeting/2020-03-09)
 * [2020-02-03](meeting/2020-02-03)
 * [2020-01-13](meeting/2020-01-13)

# 2019

 * [2019-11-25](meeting/2019-11-25)
 * [2019-11-04](meeting/2019-11-04)
 * [2019-10-07](meeting/2019-10-07)
 * [2019-09-09](meeting/2019-09-09)
 * [2019-07-01](meeting/2019-07-01)
 * [2019-06-03](meeting/2019-06-03)
 * [2019-05-06](meeting/2019-05-06)
 * [2019-04-08](meeting/2019-04-08)
 * [2019-03-04](meeting/2019-03-04)

# Templates and scripts

 * [monthly-report](meeting/monthly-report)
 * [template](meeting/template)
