This section is all old documentation imported from alberti.

<!-- update with `ls -d old/*.md | sed 's/.md$//;s/\(.*\)/ * [\1](old\/\1)/'` -->

 * [backuppc](old/backuppc)
 * [nagios-client](old/nagios-client)
 * [new-kvm-virsh-vm](old/new-kvm-virsh-vm)
 * [new-machine-cymru](old/new-machine-cymru)
 * [new-machine-nondebian](old/new-machine-nondebian)
 * [new-machine.orig](old/new-machine.orig)
 * [new-vm-rethem](old/new-vm-rethem)
