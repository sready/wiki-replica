"Retiring" a user can actually mean two things:

 * "retired", which disables their access to Tor hosts but keeps
   email working and then automatically stops after 186 days

 * "disabled", which immediately disables everything

At least, that's in theory: in practice, the userdir-ldap code seems
to just immediately disable a user when we "lock" it, so that
distinction doesn't actually exist and it is unclear where the above
actually is coming from.

# How to retire a user

Typically, the first step in retiring a user is to "lock" their user
account, which keeps them from logging in. But the user still lives in
the LDAP database, and it might be better to delete it completely. The
user also needs to be checked against all other services that might
have their own account database.

## Locking an account

So the first step is to lock the account (as in [howto/ldap](howto/ldap)):

    ssh db.torproject.org ud-lock account

A ticket number can be provided with `-r` and another state (than
"retired") can be specified with `-s`, for example:

    ud-lock -r tpo/tpa/team#666 -s inactive account

Note that this only keeps the user from accessing servers, it does
*not* remove the actual account from LDAP nor does it remove it from
the `passwd` database on servers. This is because the user might still
own files and we do not want to have files un-owned.

It also does *not* remove the email alias (the `emailForward` field in
[LDAP](howto/ldap)), for that you need to delete the account altogether.

## Deleting an account

You may also want to delete the user if it's clear they are unlikely
to come back again. For this, the actual LDAP entries for the user
must be removed with `ldapvi`, but only after the files for that user
have been destroyed or given to another user.

Note that it's unclear if we should add an email alias in the
`virtual` file when the account expires, see [ticket #32558](https://bugs.torproject.org/32558) for
details.

## Retiring from other services

Then you need to go through the [service list](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service) and pay close
attention to the services that have "authentication" enabled in the
list.

In particular, you will want to:

 1. Login as admin to [GitLab](howto/gitlab), [disable](https://docs.gitlab.com/ee/user/admin_area/moderate_users.html#deactivate-a-user) the user account, and
    remove them from critical groups. another option is to [block](https://docs.gitlab.com/ee/user/admin_area/moderate_users.html#block-a-user)
    or [ban](https://docs.gitlab.com/ee/user/admin_area/moderate_users.html#ban-and-unban-users) a user as well.
 2. Remove the user from aliases in the virtual alias map
    (`modules/postfix/files/virtual` in `tor-puppet.git`)
 3. remove the user from [mailing lists](service/lists), on eugeni:

        sudo remove_members <list names> <email address>

 4. grep for the username in `tor-puppet.git`, typically you may find
    a sudo entry
 5. remove the key from `acccount-keyring.git`

There are other manual accounts that are *not* handled by LDAP. Make
sure you check:

 * [Nextcloud](service/nextcloud)
 * [Gitolite](howto/git)
 * [Gitlab](howto/gitlab)
 * [Github](https://github.com/torproject/)

TODO: list is incomplete, need to audit [the service list](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service) and see
which services are in LDAP and which aren't. See [ticket #32519](https://bugs.torproject.org/32519).

# How to un-retire a user

To reverse the above, if the user was just "locked", you might be able
to re-enable it by doing the following:

 * delete the `accountStatus`, `shadowExpire` fields
 * add the `keyFingerprint` field matching the (trusted) fingerprint
   (from `account-keyring.git`)
 * change the user's password to something that is not locked

To set a password, you need to find a way to generate a salted UNIX
hashed password, and there are many ways to do that, but if you have a
copy of the userdir-ldap source code lying around, this could just do
it:

    >>> from userdir_ldap.ldap import HashPass, GenPass
    >>> print("{crypt}" + HashPass(GenPass()))

If the user was completely deleted from the LDAP database, you need to
restore those LDAP fields the way they were before. You can do this by
either restoring from the [howto/LDAP](howto/LDAP) database (no, that is not
fun at all -- be careful to avoid duplicate fields when you re-add
them in `ldapvi`)  OR just [create a new user](howto/create-a-new-user).
