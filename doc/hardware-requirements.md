So you want to give us hardware? Great! Here's what we need...

# Physical hardware requirements

If you want to [donate hardware][], there are specific requirements
for machine we manage that you should follow. For other donations,
please see the [donation site][].

[donate hardware]: https://donate.torproject.org/donor-faq
[donation site]: https://donate.torproject.org/

This list is not final, and if you have questions, please [contact
us](support). Also note that we also accept virtual
machine "donations" now, for which requirements are different, see
below.

## Must have

 * Out of band management with dedicated network port, preferably a
   something standard (like serial-over-ssh, with BIOS redirection),
   or failing that, serial console and networked power bars
 * No human intervention to power on or reboot
 * Warranty or post-warranty hardware support, preferably provided by
   the sponsor
 * Under the 'ownership' of Tor, although long-term loans can also
   work
 * Rescue system (PXE bootable OS or remotely loadable ISO image)

## Nice to have

 * Production quality rather than pre-production hardware
 * Support for multiple drives (so we can do RAID) although this can
   be waived for disposable servers like build boxes
 * Hosting for the machine: we do not run our own data centers or rack,
   so it would be preferable if you can also find a hosting location
   for the machine, ideally dual-stack (IPv4 and IPv6) and gigabit or
   greater

## To avoid

 * proprietary Java/ActiveX remote consoles
 * hardware RAID, unless supported with open drivers in the mainline
   Linux kernel and userland utilities

# Virtual machines requirements

## Must have

Without those, we will have to be basically convinced to accept those
machines:

 * Debian OS
 * Shell access (over SSH)
 * Unattended reboots or upgrades

The latter might require more explanations. It means the machine can
be rebooted without intervention of an operator. It seems trivial, but
some setups make that difficult. This is essential so that we can
apply Linux kernel upgrades. Alternatively, manual reboots are
acceptable if such security upgrades are automatically applied.

## Nice to have

Those we would have in an ideal world, but are not deal breakers:

 * Full disk encryption
 * Rescue OS boot to install our own OS
 * Remote console
 * Provisioning API (cloud-init, OpenStack, etc)
 * Reverse DNS
 * Real IP address (no NAT)

## To avoid

Those are basically deal breakers, but we have been known to accept
those situations as well, in extreme cases:

 * No control over the running kernel
 * Proprietary drivers
