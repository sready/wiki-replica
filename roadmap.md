# Roadmaps

We keep our plans for the future (and the paste) here.

<!-- update with `ls -d roadmap/*.md | sed 's/.md$//;s/\(.*\)/ * [\1](\1)/'` -->

 * [roadmap/2022](roadmap/2022)
 * [roadmap/2021](roadmap/2021)
 * [roadmap/2020](roadmap/2020)
