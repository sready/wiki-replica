[Discourse][] is a web platform for hosting and moderating online discussion.

The [Tor forum][] is currently hosted free of charge by Discourse.org for the
benefit of the Tor community.

[Discourse]: https://www.discourse.org/
[Tor forum]: https://forum.torproject.net/

[[_TOC_]]

# Tutorial

# How-to

## Enable new topics by email

Topic creation by email is the ability to create a new forum topic in a category
simply by sending an email to a specific address.

This feature is enabled per-category. To enable it for a category, navigate to
it, click the "wrench" icon (top right), open the **Settings** tab and scroll to
the **Email** header. Check the box next to `Accept incoming emails sent to:`.

The email address is managed automatically by Discourse, there's no need to
create it. However, the address doesn't automatically appear on the forum so to
allow users to discover it, it's a good idea to add it to the category
description.

Per the forum's settings, only users with trust level 2 (member) or higher are
allowed to post new topics by email.

## Use the app

The official companion app for Discourse is [DiscourseHub][].

Unfortunately, it doesn't appear to be available from the F-Droid repository at
the moment.

[DiscourseHub]: https://play.google.com/store/apps/details?id=com.discourse

## Mirror a mailing list

The instructions to set up a forum category that mirrors for a mailing list can
be found [here](https://meta.discourse.org/t/creating-a-read-only-mailing-list-mirror/77990#category-2).

The address that needs to be subscribed to the mailing list is
`torproject1@discoursemail.com`.

## Pager playbook

In case of a problem with the forum, we should start by checking the Discouse
[status page](https://status.discourse.org/) and the [@DiscourseStatus](https://twitter.com/DiscourseStatus) Twitter profile.

For any inquiries regarding issues with the forum, including unacknowledged
downtime, we should contact team@discourse.org immediately.

## Disaster recovery

In case of a major problem with the forum, we should contact the Discourse
people at team@discourse.org. They have access to offsite backups.

Manual backups can be triggered and retrieved via the web interface,
see [Backups](#backups). Such backups could then be used to recreate the
forum on another installation of Discourse, in case of a catastrophe.

# Reference

## Installation

The forum software was installed for us by the Discourse team, in their cloud
infrastructure, so the only thing that needed to be set up from our side was a
DNS entry.

The address chosen for this service is https://forum.torproject.net/.

It was decided to use the `.net` suffix instead of `.org` per [TPA-RFC-6](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-6-naming-convention).

### Domain rediection

For convenience, a HTTP redirection is set up in our infrastructure pointing
`forum.torproject.org` to `forum.torproject.net`.

### Onion service

We asked for this but unfortunately the Discourse team isn't interested in
setting up an onion service for `forum.torproject.net`. This may be an argument
in favor of hosting the forum on our own infrastructure down the road.

They suggested we make one available via proxy, but we didn't look into that.

## SLA

Since the forum is gratuitously hosted by Discourse for the Tor Project, the SLA
on this service is "best effort".

The [status history](https://status.discourse.org/pages/history/5e2141ce30dc5c04b3ac32fc)
suggests that outages are relatively rare.

## Design

### Akismet plugin

The Akismet plugin, enabled by default, is set to never submit data to Akismet
via the `skip akismet trust level` being set to `0:new user`.

This was discussed in a forum thread [here](https://forum.torproject.net/t/disable-akismet-anti-spam-plugin/80).

## Issues

There is no issue tracker specifically for this project, [File][] or
[search][] for issues in the [team issue tracker][search].

 [File]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new
 [search]: https://gitlab.torproject.org/tpo/tpa/team/-/issues

## Maintainer, users, and upstream

Upstream and service maintainers are [Discourse](mailto:team@discourse.org).

This service is available publicly for the benefit of the entire Tor community.

The forum is administered by the service admins which are lavamind, hiro, gus
and duncan.

## Monitoring and testing

At present TPA does not monitor `forum.discourse.net` in any way.

## Logs and metrics

Some logs and metrics are recorded by Discourse. Details are available in their
[privacy policy](https://www.discourse.org/privacy).

## Backups

This service is hosted by Discourse.org which handle creating and storing
backups for the forum:

> Off-site backups are created every 12 hours when hosted by Discourse. To access an off-site backup, or to restore an uploaded backup, please [contact us](mailto:team@discourse.org).

It's possible to trigger the creation of a downloadable backup via this [page](https://forum.torproject.net/admin/backups)
on the admin interface. Once the backup is completed, a download link is sent by
email to the admin who requested the backup.

## Deleting topics and posts

The design of Discourse is such that when a post is deleted via the web
interface, it's merely flagged as such in the database. There is no purging of
these records either, they remain for the life of the instance.

To delete a post on Discourse requires direct modification of the database, which
is obviously warned against by the developpers.

## Other documentation

 * https://meta.discourse.org/

# Discussion

## Overview

<!-- describe the overall project. should include a link to a ticket -->
<!-- that has a launch checklist -->

<!-- if this is an old project being documented, summarize the known -->
<!-- issues with the project. to quote the "audit procedure":

 5. When was the last security review done on the project? What was
    the outcome? Are there any security issues currently? Should it
    have another security review?

 6. When was the last risk assessment done? Something that would cover
    risks from the data stored, the access required, etc.

 7. Are there any in-progress projects? Technical debt cleanup?
    Migrations? What state are they in? What's the urgency? What's the
    next steps?

 8. What urgent things need to be done on this project?

-->

## Goals

<!-- include bugs to be fixed -->

### Must have

### Nice to have

### Non-Goals

## Approvals required

<!-- for example, legal, "vegas", accounting, current maintainer -->

## Proposed Solution

## Cost

## Alternatives considered

<!-- include benchmarks and procedure if relevant -->
