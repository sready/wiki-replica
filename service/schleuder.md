# Schleuder

Schleuder is a gpg-enabled mailing list manager with resending-capabilities. Subscribers can communicate encrypted (and pseudonymously) among themselves, receive emails from non-subscribers and send emails to non-subscribers via the list.

For more details see https://schleuder.org/schleuder/docs/index.html.

Schleuder runs on [Eugeni](https://db.torproject.org/machines.cgi?host=eugeni). The version of Schleuder currently installed is: 3.1.2

## Known lists

The list of schleuder list is listed alongside the [main list of
mailing lists](service/lists#list-of-mailing-lists).

## Using Schleuder

Schleuder has it's own gpg key, and also it's own keyring that you can use if you are subscribed to the list.

All command-emails need to be signed.

### Sending emails to people outside of the list

When using X-RESEND you need to add also the line X-LIST-NAME line to your email, and send it signed:

    X-LIST-NAME: listname@withtheemail.org
    X-RESEND: person@nogpgkey.org

You could also add their key to your schleuder mailing list, with

    X-LIST-NAME: listname@withtheemail.org
    X-ADD-KEY:
    [--- PGP armored block--]

And then do:

    X-LIST-NAME: listname@withtheemail.org
    X-RESEND-ENCRYPTED-ONLY: person@nogpgkey.org

### Getting the keys on a Schleuder list keyring

    X-LIST-NAME: listname@withtheemail.org
    X-LIST-KEYS

And then:

    X-LIST-NAME: listname@withtheemail.org
    X-GET-KEY: someone@important.org

## Administration of lists

There are two ways to administer schleuder lists: through the CLI interface of the schleuder API daemon, or by sending PGP encrypted emails with the appropriate commands to `listname-request@withtheemail.org`.

### Pre-requisites

#### Daemon

Mailing lists are managed through schleuder-cli which needs schleuder-api-daemon running.

The daemon is configured to start automatically, but you can verify it's running using systemctl:

    sudo systemctl status schleuder-api-daemon

#### Permissions

The `schleuder-cli` program should be executed in the context of the `torschleuder` user account:

    sudo -u torchleuder schleuder-cli

For this to work, your user account must be a member of the `torschleuder` group.

#### PGP

For administration through the `listname-request` email interface, you will need the ability to encrypt and sign messages with PGP. This can be done through your email client, or with `gpg` on the command line with the armored block them copied into a plaintext email.

All email commands must be PGP encrypted with the public key of the mailing list in question. Please follow the instructions above for obtaining that mailing list's key.

### List creation

To create a list you can:

    sudo -u torschleuder schleuder-cli lists new secret-team@lists.torproject.org admin@torproject.org /path/to/public.key

Schleuder will create the list gpg key together with the list. Please not that the created keys do not expires. For more information about how Schlueder creates keys you can check: https://0xacab.org/schleuder/schleuder/blob/master/lib/schleuder/list_builder.rb#L120

To export a list public key you can do the following:

    sudo -u torschleuder schleuder-cli keys export secret-team@lists.torproject.org <list-key-fingerprint>

### Subscriptions management

#### CLI daemon

Subscription are managed with the subscriptions command.

To subscribe a new user to a list do:

    sudo -u torschleuder schleuder-cli subscriptions new secret-team@lists.torproject.org person@torproject.org <fingerprint> /path/to/public.key

To list current list subscribers:

    sudo -u torschleuder schleuder-cli subscriptions list secret-team@lists.torproject.org

To designate (or undesignate) a list admin:

    sudo -u torschleuder schleuder-cli subscriptions set secret-team@lists.torproject.org person@torproject.org admin true

#### Email commands

Lists can also be administered via email commands sent to `listname-request@lists.torproject.org` (list name followed by `-request`). Available commands are described in the [Schleuder documentation for list-admins](https://schleuder.org/schleuder/docs/list-admins.html).

To subscribe a new user, you should first add their PGP key. To do this, send the following email to `listname-request@lists.torproject.org`, encrypted with the public key of the mailing list and signed with your own PGP key:

```
x-listname listname@lists.torproject.org
x-add-key
-----BEGIN PGP PUBLIC KEY BLOCK-----
-----END PGP PUBLIC KEY BLOCK-----
```

You should receive a confirmation email similar to the following that the key was successfully added:

```
This key was newly added:
0x1234567890ABCDEF1234567890ABCDEF12345678 user@domain.tld 1970-01-01 [expires: 2080-01-01]
```

After adding the key, you can subscribe the user by sending the following (signed and encrypted) email to `listname-request@lists.torproject.org`:

```
x-listname listname@lists.torproject.org
x-subscribe user@domain.tld 0x1234567890ABCDEF1234567890ABCDEF12345678
```

You should receive a confirmation email similar to the following:

```
user@domain.tld has been subscribed with these attributes:

Fingerprint: 1234567890ABCDEF1234567890ABCDEF12345678
Admin? false
Email-delivery enabled? true
```

### Other commands

All the other commands are available by typing:

    sudo -u torschleuder schleuder-cli help

