Tor project runs a survey services to avoid using external services that won't be privacy friendly.

# Tutorial

The website is available at <https://survey.torproject.org/>

The survey is also accessible via onion address at http://bogdyardcfurxcle.onion/

# How-to

The service uses [limesurvey](https://www.limesurvey.org/) and runs on [Subnotabile](https://db.torproject.org/machines.cgi?host=subnotabile). The [upstream user manual](https://manual.limesurvey.org/) should cover most of your needs.

## Administration

The admin interface is available under [/admin](https://survey.torproject.org/admin). You can request an admin account from an existing admin, contact information should be visible in the main page.