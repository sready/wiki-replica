---
title: TPA-RFC-23: Retire ipv6only.torproject.net
---

[[_TOC_]]

Summary: delete the ipv6only.torproject.net virtual machine on 2022-04-27

AKA: does *anyone* know what that thing even is?

# Background

While doing some cleanup, we noticed this host named
ipv6only.torproject.net in the Sunet cluster. It seems unused and is
actually shutdown, and has been for weeks.

We are migrating the boxes in this cluster to a new site, and that box
is blocking migration.

# Proposal

Formally retire ipv6only.torproject.net, which basically involves
deleting the virtual machine.

## Affected users

Unknown, probably no one.

# Approval

tor-internal.

# Deadline

The machine will be destroyed in two weeks, on 2022-04-27, unless
someone manifests themselves.

# Status

This proposal is currently in the `standard` state.

# References

See:

 * [tpo/tpa/team#40727](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40727): ipv6only retirement issue
 * [tpo/tpa/team#40684](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40684): cluster migration issue
