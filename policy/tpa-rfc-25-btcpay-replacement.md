---
title: TPA-RFC-25: BTCpay replacement
---

[[_TOC_]]

Summary: BTCpay has major maintenance issues that are incompatible
with TPA's policy. TODO: find a replacement

# Background

BTCpay has a somewhat obscure and complicated history at Tor, and is
in itself a rather complicated project. A more in-depth discussion of
the problems with the project are available in the [discussion section
of the internal documentation](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/BTCpayserver#discussion).

But a summary of the problems found during deployment are the
following:

 * PII retention and GDPR-compliance concerns (nginx logs, invoices in
   PostgreSQL)
 * upgrades require manual, periodic intervention
 * complicated design, with multiple containers and sidecars, with
   configuration generators and lots of shell scripts
 * C#/.net codebase
 * no integration with CiviCRM/donate site

# Proposal

TODO: make a proposal after evaluating different alternatives

## Requirements

TODO: clearly define the requirements, this is just a draft

### Must have

 * must accept Bitcoin payments, with confirmation
 * must not accumulate indefinitely PII
 * GDPR compliance
 * must *not* divulge a single cryptocurrency address to all visitors
   (on-the-fly generation)
 * automated upgrades
 * backup/restore procedures
 * monitoring system

### Nice to have

 * integration with CiviCRM so that payments are recorded there
 * reasonable deployment strategy
 * Prometheus integration

### Non-Goals

 * we're not making a new cryptocurrency
 * we're not offering our own "payment gateway" service (which BTCpay
   can actually provide)

## Scope

This proposal affects the processing of cryptocurrency donations from
the Tor project. 

It does not address the fundamental problems with cryptocurrencies
regarding environmental damage, ponzis schemes, fraud, and the various
security problems with cryptocurrencies, which are considered out of
scope of this proposal.

## Affected users

Fundraising, comms team, donors.

# Personas

TODO: personas

Examples:

 * ...

Counter examples:

 * ...

# Alternatives considered

## Status quo: BTCpay

TODO: expand on pros and cons of btcpay

## Simple BTC address rotation

This approach has been used by Riseup for a while. They generate a
bunch of bitcoin addresses periodically and store them on the
website. There is a button that allows visitors to request a new
one. When the list is depleted, it stops working.

TODO: expand on pros and cons of the riseup system

## Other payment software?

TODO: are there software alternatives to BTCpay?

## Commercial payment gateways

TODO: evaluate bitpay, coinbase, nowpayments.io, etc

# Costs

TODO: costs

# Approval

accounting, finance, TPA, hiro.

# Deadline

TODO: determine deadline

# Status

This proposal is currently in the `draft` state.

# References

 * internal BTCpay documentation:
   https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/BTCpayserver
 * launch ticket:
   https://gitlab.torproject.org/tpo/tpa/team/-/issues/33750
 * discussion ticket:
   https://gitlab.torproject.org/tpo/web/donate-static/-/issues/75
