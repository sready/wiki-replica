---
title: TPA-RFC-26: LimeSurvey upgrade
---

Summary: survey.torproject.org will be retired and rebuilt with a new
version, review the new instance between July 13 and 22th to avoid
data loss.

[[_TOC_]]

# Background

The `survey.torproject.org` service has been unmaintained for a long time,
during which multiple security vulnerabilities were disclosed and fixed by the
upstream LimeSurvey project.

Furthermore, our current deployment is based on LimeSurvey 3.x which
is end-of-life soon, although no specific announcement has been made
yet in that regard by the upstream developers.

# Proposal

TPA will deploy a new server with a clean LimeSurvey 5.x installation

TPA will take care of transferring the configuration (question structure only)
of previous surveys (40 total) to the new LimeSurvey instance, as well as the
creation of user accounts.

Survey authors who wish to keep user responses for one or more of their surveys
have two options:

 * Export those responses to their workstation before the retirement deadline
   (preferred)

 * Request from TPA, before July 6, in the [GitLab issue][], that the **full**
   survey, including user responses, is migrated to the new server

Survey authors who do not wish to migrate at all one or more of surveys in the
current LimeSurvey instance (eg. test surveys and such) are kindly asked to log
on to [survey.torproject.org][] and delete these surveys before July 6.

[GitLab issue]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40808
[survey.torproject.org]: https://survey.torproject.org/admin/

## Timeline

 * July 5 to 12: new LimeSurvey 5 instance deployed by TPA

 * July 13: the new instance available

 * July 22: deadline to review the surveys migrated by TPA

 * August 1st: old (LimeSurvey 3) instance shutdown

 * August 8th: old instance destroyed

 * September 1st: old instance backups destroyed

The retirement of the LimeSurvey 3 instance will destroy all survey data,
configuration and responses which have not been exported or migrated to the new
instance.

## Goals

### Must have

 * Clean LimeSurvey instance
 * Import of question structure for past surveys

### Nice to have

 * Migrate to next LTS branch before EOL

### Non-Goals

 * Audit the current LimeSurver 3.x code base and data

## Affected users

This change will affect all survey authors, including but not limited to
members of the communications, UX and community teams.

# Alternatives considered

One alternative would be to abandon self-hosting LimeSurvey and purchasing
cloud hosting for this service. According to [LimeSurvey.org pricing][]
this would cost around 191 EUR per year for the "Expert" plan which seems best
suited to our use-case, and includes the 30% discount offered to non-profits.
An important caveat with this solution is that LimeSurvey does not appear to
provide an onion service to access the surveys.

[LimeSurvey.org pricing]: https://www.limesurvey.org/pricing

# Costs

The cost of this migration is expressed here in terms of TPA labor:

| Task                       | Estimate    | Uncertainty | Note                                          | Total (days) |
|----------------------------|-------------|-------------|-----------------------------------------------|--------------|
| 1. deploy limesurvey 5.x   | 2 days      | high        | needs research                                | 4            |
| 2. survey transfer         | 1 day       | high        | possible compatibility issues                 | 2            |
| 3. retire `survey-01`      | 1 hour      | low         |                                               | 0.2          |
| **Total**                  | **3 days**  | **high**    |                                               | **6.2**      |

# Approval

This change should receive the approval of TPO project managers.

# Deadline

There is no specific deadline for this proposal but it should be processes ASAP
due to the security concerns raised by TPA about the outdated state of the
current service.

# Status

This proposal is currently in the `proposed` state.

# References

 * GitLab discussion issue: tpo/tpa/team#40808
 * original issue: tpo/tpa/team#40721
