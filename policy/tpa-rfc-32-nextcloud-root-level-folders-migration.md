---
title: TPA-RFC-32: Nextcloud root-level shared folders migration
---

[[_TOC_]]

# Background

In the Tor Project [Nextcloud][] instance, most root-level shared folders
currently exist in the namespace of a single Nextcloud user account. As such,
the management of these folders rests in the hands of a single person, instead
of the team of [Nextcloud administrators][].

In addition, there is no folder shared across all users of the Nextcloud
instance, and incoming file and folder shares are created directly in the root
of each user's account, leading to a cluttering of users' root folders. This
clutter is increasingly restricting the users' ability to use Nextcloud to its
full potential.

[Nextcloud]: https://nc.torproject.net
[Nextcloud administrators]: https://nc.torproject.net/settings/users/admin

# Proposal

## Move root-level shared folders to external storage

The first step is to activate the `External storage support` Nextcloud app.
This app is among those shipped and maintained by the Nextcloud core
developpers.

Then, in the Administration section of Nextcloud, we'll create a series of
"Local" external storage folders and configure sharing as described in the
table below:

| Current folder | New folder name      | Groups               |
| -------------- | -------------------- | -------------------- |
| ?              | Community Team       | Community Team       |
| ?              | Network Health Team  | Network Health       |
| ?              | Network Team         | Network Team         |
| ?              | Applications Team    | Applications Team    |
| ?              | Grants Team          | Grants Team          |
| ?              | UX Team              | UX Team              |
| ?              | Anti-censorship Team | Anti-censorship Team |
| ?              | Communications Team  | Communications       |
| ?              | Metrics Team         | Metrics Team         |
| Sysadmin       | TPA Team             | TPA Team             |

## Create "TPI" and  "Common" shared folders

We'll create a shared folder named "Common", shared with all Nextcloud user
groups, and a "TPI" folder shared with all TPI employees and contrators.

 * **Common** would serve as a repository for documents of general interest to
   the Tor Project community, and a common space to share documents that have
   no specific confidentiality requirements

 * **TPI** would host documents of interest to TPI personnel, such as holiday
   calendars and the employee handbook

## Set system-wide default incoming shared folder to "Incoming"

Currently when a Nextcloud user shared documents or folders with another user
or group of users, those appear in the share recipients' root folder.

By making this change in the Nextcloud configuration (`share_folder`
parameter), users who have not already changed this in their personal
preferences will receive new shares in that subfolder, instead of the root
folder. It will not move existing files and folders, however.

## Goals

 * Streamline the administration of team shared folders
 * De-clutter users' Nextcloud root folder

## Scope

The scope of this proposal is the Nextcloud instance at
https://nc.torproject.net

## Affected users

All users of the Nextcloud instance.

Our Riseup counterpart for Nextcloud administration will be required to
manually create a handful of folder on the server.

# Costs

There are no costs associated with this proposal, apart from the labor costs.

# Approval

This proposal should be approved by the current team of Nextcloud
administrators.

# Status

This proposal is currently in the `draft` state.

# References

 * tpo/tpa/nextcloud#1
