---
title: TPA-RFC-35: GitLab email address changes
---

[[_TOC_]]

Summary: headers in GitLab email notifications are changing, you may
need to update your email filters

# Background

I am working on building a development server for GitLab, where we can
go wild testing things without breaking the production
environment. For email to work there, I need a configuration that is
separate from the current production server.

Unfortunately, the email address used by the production GitLab server
doesn't include the hostname of the server (`gitlab.torproject.org`)
and only the main domain name (`torproject.org`) which makes it
needlessly difficult to add new configurations.

Finally, using the full service name (`gitlab.torproject.org`)
address means that the GitLab server will be able to keep operating
email services even if the main email service goes down.

It's also possible the change will give outgoing email better
reputation with external spam filters, because the domain part of the
`From:` address will actually match the machine actually sending the
email, which wasn't the case when sending from `torproject.org`.

# Proposal

This changes the headers:

    From: gitlab@torproject.org
    Reply-To: gitlab-incoming+%{key}@torproject.org

to:

    From: git@gitlab.torproject.org
    Reply-To: git+%{key}@gitlab.torproject.org

If you are using the `From` headers in your email client filters, for
example to send all GitLab email into a separate mailbox, you WILL
need to make a change for that filter to work again. I know I had to
make such a change, which was simply to replace
`gitlab@torproject.org` by `git@gitlab.torproject.org` in my filter.

The `Reply-To` change should not have a real impact. I suspected
emails sent before the change might not deliver properly, but I tested
this, and both the old emails and the new ones work correctly, so that
change should be transparent to everyone.

(The reason for that is that the previous
`gitlab-incoming@torproject.org` address is *still* forwarding to
`git@torproject.org` so that will work for the foreseeable future.)

# Alternatives considered

## Reusing the prod email address

The main reason I implemented this change is that I want to have a
GitLab development server, as mentioned in the background. But more
specifically, we don't want the prod and dev servers to share email
addresses, because then people could easily get confused as to where a
notification is coming from. Even worse, a notification from the dev
server could yield a reply that would end up in the prod server.

## Adding a new top-level address

So, clearly, we need two different email addresses. But why change the
*current* email address instead of just adding a new one? That's
trickier. One reason is that I didn't want to add a new alias on the
top-level `torproject.org` domain. Furthermore, the old configuration
(using `torproject.org`) is officially [discouraged upstream][] as it
can lead to some security issues.

[discouraged upstream]: https://docs.gitlab.com/ee/administration/incoming_email.html#security-concerns

# Costs

N/A, staff.

# Approval

This needs approval from TPA and tor-internal.

# Deadline

This will be considered approved tomorrow (2022-06-30) at 16:00 UTC
unless there are any objections, in which case it will be rolled back
for further discussion.

The reason there is such a tight deadline is that I want to get the
development server up and running for the Hackweek. It is proving less
and less likely that the server will actually be *usable* *during* the
Hackweek, but if we can get the server up as a result of the Hackweek,
it will already be a good start.

# Status

This proposal is currently in the `proposed` state.

# References

Comments welcome by email or in issue [tpo/tpa/team#40820][].

[tpo/tpa/team#40820]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40820
