---
title: TPA-RFC-6: Naming Convention
---

Summary: naming things is hard, but should at least be
consistent. This policy documents how domain names are used, how to
name machines, services, networks and might eventually document IP
addressing as well.

[[_TOC_]]

# Domain names

Tor uses two main domain names for things:

 * `torproject.org`
 * `torproject.net`

There might be other domains managed by us or registered in the DNS,
but they should eventually point to one of those, generally
`torproject.org`.

All TPA-managed machines and services on those machines should be
under `torproject.org`. The naming scheme of the individual machines
is detailed below. This is managed by TPA directly through
[howto/dns](howto/dns).

External services and machines *can* be hosted under
`torproject.net`. In that case, the only association is a `CNAME` or
`A` record pointing to the other machine. To get such a record,
contact TPA using the normal communication channels detailed in
[support](support).

# Machine names

There are multiple naming schemes in use:

 * onion species
 * role-based
 * location-based

We are trying to phase out the onion-based names, in favor of more
*descriptive* names. It kind of takes the soul out of the
infrastructure, but it makes things much easier to figure out for
newcomers. It also scales better.

## Onion species

Note that this naming scheme is deprecated. Favor role-based names,
see below.

[Wikipedia list of onion species][], preferably picking a first letter
matching purpose (e.g. "m" for monitoring, "b" for backups, "p" for
puppet) and ideally not overlapping with [existing machines at
debian.org][] in the first three letters or at least the short
hostname part

[Wikipedia list of onion species]: https://en.wikipedia.org/wiki/List_of_Allium_species
[existing machines at debian.org]: https://db.debian.org/machines.cgi

> Example: monticola.torproject.org was picked as a "monitoring"
> ("mon") server to run the experimental Prometheus server. no
> machine is named "monticola" at Debian.org and no machine has
> "mon" or smaller as its first three letters there either.

## Roles

Another naming scheme is `role-ID`, where:

 * `role` is what the server is for, for example `gitlab`, `mon` for
   monitoring, `crm`, etc. try to keep it short and abbreviate to
   at most three letters if role is longer than five. `role` might
   have a dash (`-`) in it to describe the service better (`crm-ext`
   vs `crm-int`)
 * `ID` is a two-character number, padded with zero, starting from
   one, to distinguish between multiple instances of the same server
   (e.g. `mon-01`, `mon-02`)

## Location

Note that this naming scheme is deprecated. Favor role-based names,
see above.

Another naming scheme used for virtual machines is `hoster-locN-ID`
(example `hetzner-hel1-01`), where:

 * `hoster`: is the hosting provider (example `hetzner`)
 * `locN`: is the three-letter code of the city where the machine is
   located, followed by a digit in case there are multiple locations
   in the same city (e.g. `hel1`)
 * `ID`: is an two-character number, padded with zero, starting from
   one, to distinguish multiple instances at the same location

This is used for virtual machines at Hetzner that are bound to a
specific location.

# Network names

Networks also have names. The network names are used in reverse DNS to
designate network, gateway and broadcast addresses, but also in
[howto/ganeti](howto/ganeti), where networks are managed automatically for virtual
machines.

Future networks should be named `FUN-LOCNN-ID` (example
`gnt-fsn13-02`) where:

 * `FUN` is the function (e.g. `gnt` for [howto/ganeti](howto/ganeti))
 * `LOCNN` is the location (e.g. `fsn13` for Falkenstein)
 * `ID` is a two-character number, padded with zero, starting from
   one, to distinguish multiple instances at the same
   function/location pair

The first network was named `gnt-fsn`, for `Ganeti in the Falkenstein
datacenter`. That naming convention is considered a legacy exception
and should not be reused. It might be changed in the future.

# Deadline

Considering this documentation has been present in the wiki for a
while, it is already considered adopted. The change to deprecate the
location and onions names was informally adopted some time in 2020.

# Status

This proposal is currently in the `standard` state.

# References

 * [RFC1178](https://tools.ietf.org/html/rfc1178): "Choosing a Name for Your Computer", August 1990
 * [RFC2100](https://tools.ietf.org/html/rfc2100): "The Naming of Hosts", 1 April 1997
 * [Wikipedia: Computer network naming scheme](https://en.wikipedia.org/wiki/Computer_network_naming_scheme)
 * <https://namingschemes.com/>
 * [Another naming scheme](https://mnx.io/blog/a-proper-server-naming-scheme/)
 * [Location code names from the UN](https://unece.org/trade/cefact/unlocode-code-list-country-and-territory)
