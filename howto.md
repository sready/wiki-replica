# Howtos

This documentation is primarily aimed at sysadmins and establishes
various procedures not necessarily associated with a specific service.

<!-- update with `ls -d howto/*.md | sed 's/.md$//;s/\(.*\)/ * [\1](howto\/\1)/'` -->

 * [benchmark](howto/benchmark)
 * [build_and_upload_debs](howto/build_and_upload_debs)
 * [create-a-new-user](howto/create-a-new-user)
 * [cumin](howto/cumin)
 * [fabric](howto/fabric)
 * [incident-response](howto/incident-response)
 * [lektor](howto/lektor)
 * [lvm](howto/lvm)
 * [new-machine](howto/new-machine)
 * [new-machine-cymru](howto/new-machine-cymru)
 * [new-machine-hetzner-cloud](howto/new-machine-hetzner-cloud)
 * [new-machine-hetzner-robot](howto/new-machine-hetzner-robot)
 * [new-machine-mandos](howto/new-machine-mandos)
 * [new-person](howto/new-person)
 * [openpgp](howto/openpgp)
 * [puppet](howto/puppet)
 * [raid](howto/raid)
 * [retire-a-host](howto/retire-a-host)
 * [retire-a-user](howto/retire-a-user)
 * [upgrades](howto/upgrades)
 * [yubikey](howto/yubikey)

## old

Those are old docs that should be destroyed or merged above.

<!-- update with `ls -d old/*.md | sed 's/.md$//;s/\(.*\)/ * [\1](old\/\1)/'` -->

 * [new-kvm-virsh-vm](old/new-kvm-virsh-vm)
 * [new-machine-nondebian](old/new-machine-nondebian)
 * [new-machine.orig](old/new-machine.orig)
 * [new-vm-rethem](old/new-vm-rethem)
