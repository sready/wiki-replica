# Policies

The policies below document major architectural decisions taken in the
history of the team. This process is similar to the [Network Team Meta
Policy][]. More details of the process is available in the first
policy, [tpa-rfc-1-policy](policy/tpa-rfc-1-policy).

[Network Team Meta Policy]: https://gitlab.torproject.org/legacy/trac/-/wikis/org/teams/NetworkTeam/MetaPolicy

To add a new policy, create the page using the [template](policy/template)
and add it to the above list.

## Draft

 * [TPA-RFC-3: tools](policy/tpa-rfc-3-tools)
 * [TPA-RFC-11: SVN retirement](policy/tpa-rfc-11-svn-retirement)
 * [TPA-RFC-16: Replacing lektor-i18n-plugin](policy/tpa-rfc-16-replacing-lektor-i18n-plugin)
 * [TPA-RFC-17: Disaster recover](policy/tpa-rfc-17-disaster-recovery)
 * [TPA-RFC-18: Security policy](policy/tpa-rfc-18-security-policy)
 * [TPA-RFC-25: BTCpay replacement](policy/tpa-rfc-25-btcpay-replacement)
 * [TPA-RFC-31: outsource email services](policy/tpa-rfc-31-outsource-email)
 * [TPA-RFC-32: Nextcloud root-level shared folders migration](policy/tpa-rfc-32-nextcloud-root-level-folders-migration)
 * [TPA-RFC-33: Monitoring](policy/tpa-rfc-33-monitoring)

## Proposed

<!-- No policy is currently `proposed`. -->

 * [TPA-RFC-26: LimeSurvey upgrade](policy/tpa-rfc-26-limesurvey-upgrade)
 * [TPA-RFC-35: GitLab email address changes](policy/tpa-rfc-35-gitlab-email-address-changes)

## Standard

 * [TPA-RFC-1: RFC process](policy/tpa-rfc-1-policy)
 * [TPA-RFC-2: Support](policy/tpa-rfc-2-support)
 * [TPA-RFC-5: GitLab migration](policy/tpa-rfc-5-gitlab)
 * [TPA-RFC-6: Naming Convention](policy/tpa-rfc-6-naming-convention)
 * [TPA-RFC-7: root access](policy/tpa-rfc-7-root)
 * [TPA-RFC-8: GitLab CI libvirt exception](policy/tpa-rfc-8-gitlab-ci-libvirt)
 * [TPA-RFC-13: Use OKRs for the 2022 roadmap](policy/tpa-rfc-13-okrs-for-roadmap)
 * [TPA-RFC-14: GitLab artifacts expiry](policy/tpa-rfc-14-gitlab-artifacts)
 * [TPA-RFC-19: GitLab labels](policy/tpa-rfc-19-gitlab-labels)
 * [TPA-RFC-20: bullseye upgrade schedule](policy/tpa-rfc-20-bullseye-upgrades)
 * [TPA-RFC-22: rename TPA IRC channel and Matrix bridge](policy/tpa-rfc-22-rename-irc)
 * [TPA-RFC-24: Extend merge permissions for web projects](policy/tpa-rfc-24-extend-merge-permissions-web)
 * [TPA-RFC-27: Python 2 end of life](policy/tpa-rfc-27-python2-eol)
 * [TPA-RFC-29: Lektor SCSS Plugin](policy/tpa-rfc-29-lektor-scss-plugin)
 * [TPA-RFC-30: Changing how lego plugins are used](policy/tpa-rfc-30-change-lego-plugins)

## Rejected

 * [TPA-RFC-15: Email services](policy/tpa-rfc-15-email-services) (replaced with TPA-RFC-31)

## Obsolete

 * [TPA-RFC-4: Prometheus disk space change](policy/tpa-rfc-4-prometheus-disk) (one-time change)
 * [TPA-RFC-9: "proposed" status and small process changes](tpa-rfc-9-proposed-process) (merged
   in TPA-RFC-1)
 * [TPA-RFC-10: Jenkins retirement](policy/tpa-rfc-10-jenkins-retirement) (one-time change)
 * [TPA-RFC-12: triage and office hours](policy/tpa-rfc-12-triage-and-office-hours) (merged in TPA-RFC-2)
 * [TPA-RFC-21: uninstall SVN](policy/tpa-rfc-21-uninstall-svn) (one-time change)
 * [TPA-RFC-23: retire ipv6only.torproject.net](policy/tpa-rfc-23-retire-ipv6only) (one-time change)
 * [TPA-RFC-28: Alphabetical triage star of the week](policy/tpa-rfc-28-alphabetical-triage) (merged in TPA-RFC-2)
 * [TPA-RFC-34: End of office hours](policy/tpa-rfc-34-office-hours-ends) (merged in TPA-RFC-2)
