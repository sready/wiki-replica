# this is a pre-processing job that runs inside a git-enabled
# container
#
# it is designed to run when a new commit is pushed to the repository,
# not merge requests, for which there is a separate job below.
#
# it finds the modified files to make sure we only run the linter on
# those files. it uses a separate image because
# markdownlint/markdownlint doesn't ship with git (and runs as a
# regular user, so we can't install it either)
find-files-commit:
  stage: build
  image: debian:stable-slim
  script:
    - apt update && apt install -yy --no-install-recommends git ca-certificates
    - echo "commit SHA $CI_COMMIT_SHA"
    - |
      echo "working on files... $(git diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA | tee changed-files.txt)"
  except:
    refs:
      - merge_requests
  only:
    changes:
      - "*.md"
      - "**/*.md"
  artifacts:
    paths:
      - changed-files.txt
    expose_as: 'changed files'

# this is the same as the above, but for merge requests
find-files-mr:
  stage: build
  image: debian:stable-slim
  script:
    - apt update && apt install -yy --no-install-recommends git ca-certificates
    - echo "MR target SHA $CI_MERGE_REQUEST_TARGET_BRANCH_SHA"
    - |
      echo "working on files: $(git diff-tree --no-commit-id --name-only -r $CI_MERGE_REQUEST_TARGET_BRANCH_SHA | tee changed-files.txt)"
  only:
    refs:
      - merge_requests
    changes:
      - "*.md"
      - "**/*.md"
  artifacts:
    paths:
      - changed-files.txt
    expose_as: 'changed files'

# this compares the current repo with the actual wiki to make sure
# we're not missing any commits, and will fail the push if we need to
# pull from the wiki
#
# it doesn't run on merge requests to leave those poor people alone
fail-on-desync-wiki:
  stage: build
  image: debian:stable-slim
  script:
    - apt update && apt install -yy --no-install-recommends git ca-certificates
    - git fetch wiki || git remote add -f wiki https://gitlab.torproject.org/tpo/tpa/team.wiki.git
    - git merge --ff-only wiki/master
  except:
    refs:
      - merge_requests

# this runs after the "build" stage above, and consumes the
# `changed-files.txt` artifact from that stage, regardless of the job
# which generated it.
test:
  image:
    name: markdownlint/markdownlint
    entrypoint: ["/bin/bash", "-c"]
  script:
    - |
      echo "working on files: $(cat changed-files.txt)"
    - ./bin/mdl-wrapper $(cat changed-files.txt)
  only:
    changes:
      - "*.md"
      - "**/*.md"

# this will simply run all the time, regardless of which files
# changed, so it doesn't require the above
testall:
  image:
    name: markdownlint/markdownlint
    entrypoint: ["/bin/bash", "-c"]
  script:
    - echo 'this is important to get the return value of mdl, not grep'
    - set -o pipefail
    - |
      mdl . | ( grep -v "Kramdown Warning: No link definition for link ID '\[\?_toc_\]\?' found on line" || true )
  # this could be turned into allow_failures:exit_codes 2 when
  # everything but [[_toc_]] is fixed (because that will never be)
  allow_failure: true
